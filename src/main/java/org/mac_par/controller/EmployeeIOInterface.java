package org.mac_par.controller;

import java.io.File;
import java.util.List;

import org.mac_par.model.Employee;

import javafx.collections.ObservableList;

public interface EmployeeIOInterface {
	void readFromFile(ObservableList<Employee> employeeList);
	void saveFile(final List<Employee> list, boolean bRapporting);
}
