package org.mac_par.controller;
import javafx.scene.image.ImageView;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.fxml.FXML;


public class DialogPromptController {
	@FXML private ImageView imageView;
	@FXML private Label label;
	
	public void setInfoPrompt(String msg) {
		imageView.setImage(new Image(DialogPromptController.class.getResource("/images/info.png").toExternalForm()));
		label.setText(msg);
	}
	
	public void setWarningPrompt(String msg) {
		imageView.setImage(new Image(DialogPromptController.class.getResource("/images/warning.png").toExternalForm()));
		label.setText(msg);
	}
	
	public void close() {
		this.imageView=null;
		this.label=null;
	}
}
