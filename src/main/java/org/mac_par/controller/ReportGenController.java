package org.mac_par.controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import java.util.regex.Pattern;
import org.mac_par.model.Employee;
import javafx.stage.Stage;
import org.mac_par.view.DialogPrompt;

public class ReportGenController {
	//table elements
	@FXML private TableView<Employee> mainTable;
	@FXML private TableColumn<Employee,String> firstNameColumn;
	@FXML private TableColumn<Employee,String> lastNameColumn;
	@FXML private TableColumn<Employee,Integer> roomNbrColumn;
	//textfields
	@FXML private TextField firstNameField;
	@FXML private TextField lastNameField;
	@FXML private TextField roomNbrField;
	@FXML private TextField startHourField;
	@FXML private TextField endHourField;
	//container
	private ObservableList<Employee> empList;
	@FXML private Canvas officeLayout;
	private EmployeeIOInterface fileManager;
	private static final String strPattern="^((0[1-9]{1}|1[0-9]{1}|2[0-3]{1}):(0[0-9]{1}|1[0-9]{1}|2[0-9]{1}|3[0-9]{1}|4[0-9]{1}|5[0-9]{1}))$";
	private OfficeLayoutController layoutController;
	
	
	public void setStage(Stage stageInstance) {
		if(stageInstance!=null) {
			this.fileManager=new EmployeeIOManager(stageInstance);
		}
	}
	
	public void initialize() {
		empList=FXCollections.observableArrayList();
		mainTable.setItems(this.empList);
		this.layoutController=new OfficeLayoutController(this.officeLayout);
		//set columns
		this.firstNameColumn.setCellValueFactory(new PropertyValueFactory("firstName"));
		this.lastNameColumn.setCellValueFactory(new PropertyValueFactory("lastName"));
		this.roomNbrColumn.setCellValueFactory(new PropertyValueFactory("roomNbr"));
		this.mainTable.getColumns().setAll(this.firstNameColumn,this.lastNameColumn,this.roomNbrColumn);
		//set event handling
		setEvents();
	}
	
	private void setEvents() {
		this.mainTable.getSelectionModel().selectedItemProperty().addListener((obsv,oldValue,newValue)->{
			setFieldsByEmployee(newValue);
			updateOfficeLayout(newValue);
		});
	}
	
	@FXML 
	private void readInReport() {
		synchronized(this.empList) {
			if(this.fileManager!= null) {
				this.empList.clear();
				this.fileManager.readFromFile(this.empList);
				//if data was retrieved update office layout
				if(this.empList.size()>0)
					layoutController.updateLayout(this.empList.size());
			}
		}
	}
	@FXML 
	private void saveReport() {
		if(this.fileManager!=null) {
			this.fileManager.saveFile(this.empList, false);
		}
	}
	@FXML 
	private void addNewEmployee() {
		
			try{
				String firstName=this.firstNameField.getText().trim();
				String lastName=this.lastNameField.getText().trim();
				Integer room=Integer.parseInt(this.roomNbrField.getText().trim());
				
				
				Pattern pattern=Pattern.compile(strPattern);
				String start=this.startHourField.getText();
				String end=this.endHourField.getText();
				boolean bAdded=false;
				if(firstName!=null && lastName!=null && pattern.matcher(start).matches()
						&& pattern.matcher(end).matches()) {
					Employee tmpEmployee=new Employee(firstName,lastName,room,start,end);
					
					if(!this.isPresent(tmpEmployee)) {
						bAdded=true;
						this.empList.add(tmpEmployee);
						mainTable.refresh();
						updateOfficeLayout(tmpEmployee);
					}
				}
				//inform user about employee adding/modifying process result
				if(bAdded) {
					String msg=String.format("Employee \"%s %s\" was succesfully added/modified", firstName,lastName);
					DialogPrompt.displayInfo(msg);
				}else {
					DialogPrompt.displayWarning("Adding/modifying could not be finalized. Please check correctness of inputed data.");
				}
			}catch(Exception ex) {
				System.out.println(ex.getMessage());
				return;
			}
			
	}
	@FXML 
	private void generateReport() {
		if(this.fileManager!=null) {
			this.fileManager.saveFile(this.empList, true);
		}
	}
	
	private void updateOfficeLayout(Employee employee) {
		layoutController.drawOffice();
		layoutController.addRooms(empList, employee);
	}
	
	private void setFieldsByEmployee(final Employee employee) {
		if(employee==null)
			return;
		
		this.firstNameField.setText(employee.getFirstName());
		this.lastNameField.setText(employee.getLastName());
		this.roomNbrField.setText(employee.getRoomNbr().toString());
		this.startHourField.setText(employee.getStartWorkTime());
		this.endHourField.setText(employee.getEndWorkTime());
	}
	
	private boolean isPresent(final Employee employee) {
		for(Employee emp: this.empList) {
			if(emp.equals(employee)) {
				if(Integer.compare(emp.getRoomNbr(), employee.getRoomNbr())!=0) {
					emp.setRoomNbr(employee.getRoomNbr());
					this.mainTable.refresh();
				}
				
				return true;
			}
		}
		return false;
	}
	
	
	public void close() {
		//clear dialog prompt
		DialogPrompt.close();
		//clear the table content
		for(int i=0;i<this.mainTable.getItems().size();i++)
			this.mainTable.getItems().clear();
		//clear data container
		this.empList.clear();
	}
}
