package org.mac_par.controller;
import org.mac_par.model.Employee;
import javafx.stage.Stage;
import javafx.stage.FileChooser;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import java.util.Date;
import org.mac_par.view.DialogPrompt;

public class EmployeeIOManager implements EmployeeIOInterface{
	private final static String EXT=".empl";
	//format year2 month2 day2 _hour2min2
	private final static String DEFAULT_REPORT_FILENAME= "EMPLOYEE_REPORT_%ty%<tm%<td_%<tR"+EXT;
	private final static String REPORT_DIR="EMPLOYEE_REPORTS";
	private File fileCurrent=null;
	private File fileDefaultReportDir;
	private final Stage primaryStage;
	private final FileChooser fileChooser;
	
	public EmployeeIOManager(Stage stage) {
		this.primaryStage=stage;
		this.fileChooser=new FileChooser();
		init();
	}
	
	private void init() {
		setUpFileChooser();
		setUpFileDefaultDir();
	}
	
	private void setUpFileChooser() {
		this.fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		this.fileChooser.setInitialFileName("untitled"+EXT);
		this.fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("EmployeeData file","*"+EXT));
	}
	
	private void setUpFileDefaultDir() {
		this.fileDefaultReportDir=new File(System.getProperty("user.home"),REPORT_DIR);
		if(!this.fileDefaultReportDir.exists()) {
			this.fileDefaultReportDir.mkdirs();
		}
	}
	
	@Override
	public void readFromFile(ObservableList<Employee> employeeList){
		//let user to choose the file
		this.fileChooser.setTitle("Choose an EmployeeData file");
		this.fileCurrent= this.fileChooser.showOpenDialog(this.primaryStage);
		if(this.fileCurrent!=null) {
			List<Employee> list = readFromFile(this.fileCurrent);
			if(list !=null && list.size()>0) {
				employeeList.addAll(list);
			}
			boolean result=list!=null&&list.size()>0;
			this.showDialog(result, (result)?"Data was succesfully read from file":"Data could not "
					+ "be retrieved.\nPerhaps, data source was corrupted or in wrong format");
		}
	}	
	
	private List<Employee> readFromFile(File file) {
		List<Employee> list=new LinkedList<>();;
		
		try(Scanner scn=new Scanner(new FileInputStream(file))) {
			scn.useDelimiter("\\s");
			while(scn.hasNext()) {
				String name=scn.next();
				String surname=scn.next();
				int room=scn.nextInt();
				String startHour=scn.next();
				String endHour=scn.next();
				list.add(new Employee(name,surname,room,startHour,endHour));
			}
			return list;
		}catch(FileNotFoundException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "readFromFile: File", ex);
			this.showDialog(false,"File could not be found");
			return list;
		}
		catch(Exception ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "readFromFile:", ex);
			this.showDialog(false,"Error has occured: "+ex.getMessage());
			return list;
		}
	}
	
	private boolean saveToFile(File file, final List<Employee> list) {
		boolean bSucceded=false;
		try(BufferedWriter bw=new BufferedWriter(new PrintWriter(file))){
			for(Employee emp: list) {
				bw.write(emp.toString());
			}
			bw.flush();
			bSucceded=true;
		}
		catch(IOException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "saveToFile - IO", ex);
			this.showDialog(false, "An error has occured while writing to a file: "+ex.getMessage());
		}
		catch(NullPointerException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "saveToFile - NULL", ex);
		}
		finally {
			return bSucceded;
		}
	}
	
	@Override
	public void saveFile(final List<Employee> list, boolean bRapporting) {
		boolean bResult=false;
		if(bRapporting) {
			List<Employee> listTmp=new ArrayList<>(list);
			//sort
			sortList(listTmp);
			String strReport=String.format(DEFAULT_REPORT_FILENAME, new Date());
			bResult= saveToFile(new File(this.fileDefaultReportDir,strReport),listTmp);
			if(bResult) {
				this.showDialog(bResult, "Raport was saved under directory: "+this.fileDefaultReportDir.getPath());
			}
		}else {
			bResult= saveToFile(this.fileCurrent,list);
			if(bResult) {
				this.showDialog(bResult, "Succesfully saved to file: "+this.fileCurrent.getName()+
						"\nunder: "+this.fileCurrent.getParent());
			}
		}
	}
	
	protected void sortList(List<Employee> list) {
		for(int i=1;i<list.size();i++) {
			int current=i;
			int prev=i-1;
			while(prev>=0 && (list.get(prev).compareTo(list.get(current))>0)) {
				Employee tmp=list.get(current);
				list.set(current, list.get(prev));
				list.set(prev,tmp);
				current--;
				prev--;
			}
		}
	}
	public void close() {
		this.fileCurrent=null;
		this.fileDefaultReportDir=null;
	}
	
	private void showDialog(boolean bInfo,String message) {
		if(bInfo) {
			DialogPrompt.displayInfo(message);
		}else {
			DialogPrompt.displayWarning(message);
		}
	}
}