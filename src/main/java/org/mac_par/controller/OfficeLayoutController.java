package org.mac_par.controller;
import java.util.SortedSet;
import java.util.Collection;
import java.util.TreeSet;
import org.mac_par.model.Employee;
import javafx.scene.canvas.Canvas;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineJoin;

public class OfficeLayoutController {
	private static final String KEY="%s:%s";
	private static final double MARGIN=0.02;
	private static final double DEFAULT_STROKE_WIDTH=1.0;
	private final Canvas mCanvas;
	private Dimension2D officeSpace;
	private Point2D officeStartPoint;
	private static final int ROOMS_IN_A_ROW=6;
	private double dblVCorridorWidth;
	private double dblHCorridorWidth;
	private double dblRoomWidth;
	private double dblRoomHeight;
	private int rows;
	
	public OfficeLayoutController(Canvas canvas) {
		this.mCanvas=canvas;
		this.init();
	}
	
	private void init() {
		this.officeStartPoint=new Point2D(this.mCanvas.getWidth()*MARGIN,this.mCanvas.getHeight()*MARGIN);
		this.officeSpace=new Dimension2D(this.mCanvas.getWidth()-this.officeStartPoint.getX()*2,
				this.mCanvas.getHeight()-this.officeStartPoint.getY()*2);
		
		this.dblVCorridorWidth=(this.officeSpace.getWidth()/ROOMS_IN_A_ROW)*0.33;
		this.dblHCorridorWidth=0;
		this.dblRoomWidth=(this.officeSpace.getWidth()-this.dblVCorridorWidth*2)/ROOMS_IN_A_ROW;
		this.dblRoomHeight=dblRoomWidth;
		this.rows=0;
	}
	
	public void addRooms(final Collection<Employee> employeeCollection,final Employee currentEmpl) {
		if(employeeCollection== null || employeeCollection.size()==0)
			return;
		SortedSet<Employee> sorted=new TreeSet<>((o1,o2)->{
			return Integer.compare(o1.getRoomNbr(), o2.getRoomNbr());
		});
		sorted.addAll(employeeCollection);
		setLayout(sorted.size());
		synchronized(employeeCollection) {

		int position=0;
			for(Employee emp: sorted) {
				if(currentEmpl.equals(emp)) {
					break;
				}
				position++;
			}
		
			int thisRow=position/ROOMS_IN_A_ROW;
			int thisColumn=position%ROOMS_IN_A_ROW;
			//highlight an office room
			highlightRoom(thisRow, thisColumn);
			
		}
	}
	
	public void updateLayout(int size) {
		setLayout(size);
		drawOffice();
	}
	
	
	private void setLayout(int size) {
		if(Integer.compare(size, 0)==0)
			return;
		if(size/ROOMS_IN_A_ROW==this.rows)
			return;
		this.rows=size/ROOMS_IN_A_ROW;
		this.rows++;
		//calculate horizontal corridor width
		this.dblHCorridorWidth=(this.officeSpace.getHeight()/this.rows)*0.33;
		//room height-corridor*(rows-1)
		this.dblRoomHeight=(this.officeSpace.getHeight()-this.dblHCorridorWidth*(this.rows-1))/this.rows;
		
	}
	
	private void highlightRoom(int row,int column) {
		double posY=row*(this.dblRoomHeight+this.dblHCorridorWidth)+this.officeStartPoint.getX();
		double posX=column*(this.dblRoomWidth)+((int)(column/2))*this.dblVCorridorWidth+this.officeStartPoint.getY()
		;
		GraphicsContext gc=mCanvas.getGraphicsContext2D();
		gc.setFill(Color.GREENYELLOW);
		gc.setStroke(Color.BLACK);
		gc.fillRect(posX, posY, this.dblRoomWidth, this.dblRoomHeight);
		gc.strokeRect(posX, posY, this.dblRoomWidth, this.dblRoomHeight);
	}
	
	public void drawOffice() {
		GraphicsContext gc=this.mCanvas.getGraphicsContext2D();
		gc.setStroke(Color.BLACK);
		gc.setLineWidth(DEFAULT_STROKE_WIDTH);
		gc.setLineJoin(StrokeLineJoin.BEVEL);
		//draw main office space
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, this.mCanvas.getWidth(), this.mCanvas.getHeight());
		//draw openspace
		gc.setFill(Color.GREY);
		gc.fillRect(this.officeStartPoint.getX(), this.officeStartPoint.getY(),
				this.officeSpace.getWidth(), this.officeSpace.getHeight());
		//drow offices
		gc.setFill(Color.WHITESMOKE);
		double startX=this.officeStartPoint.getX();
		double startY=this.officeStartPoint.getY();
		
		//draw offices
		for(int x=0;x<ROOMS_IN_A_ROW;x++) {
			double posX=x*this.dblRoomWidth+startX;
			for(int y=0;y<rows;y++) {
				int corridorX=x/2;
				gc.fillRect(posX+corridorX*this.dblVCorridorWidth, startY+y*(this.dblRoomHeight)+y*dblHCorridorWidth,
						this.dblRoomWidth, this.dblRoomHeight);
				gc.strokeRect(posX+corridorX*this.dblVCorridorWidth, startY+y*(this.dblRoomHeight)+y*dblHCorridorWidth,
						this.dblRoomWidth, this.dblRoomHeight);
				}				
		}
	}
	
	public void clear() {
		this.officeSpace=null;
		this.officeStartPoint=null;
		this.dblVCorridorWidth=0;
		this.dblHCorridorWidth=0;
		this.dblRoomWidth=0;
		this.dblRoomHeight=0;
		this.rows=0;
	}
}
