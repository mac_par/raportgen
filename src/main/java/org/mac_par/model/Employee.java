package org.mac_par.model;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.logging.Logger;
import java.lang.Comparable;


public class Employee implements Comparable<Employee> {
	private final ReadOnlyStringProperty firstNameProperty;
	private final ReadOnlyStringProperty lastNameProperty;
	private final IntegerProperty roomNbrProperty;
	private LocalTime startWorkTime;
	private LocalTime endWorkTime;
	private static final String TIME_FORMAT = "HH:mm";
	private static final String DEFAULT_START_TIME = "08:00";
	private static final long MINIMAL_WORK_TIME = 8;
	private static final double MINUTES_PER_HOUR=60.0d;
	private static final DateTimeFormatter format = DateTimeFormatter.ofPattern(Employee.TIME_FORMAT,
			Locale.getDefault());

	// imie nazwisko nr pokoju, czas rozpoczecia czas zakonczenia

	public Employee(String firstName, String lastName, int roomNbr, String startTime, String endTime) {
		this.firstNameProperty = new ReadOnlyStringWrapper(this, "firstNameProperty", firstName).getReadOnlyProperty();
		this.lastNameProperty = new ReadOnlyStringWrapper(this, "lastNameProperty", lastName).getReadOnlyProperty();
		this.roomNbrProperty = new SimpleIntegerProperty(this, "roomNbrProperty", roomNbr);
		setTime(startTime, endTime);
	}

	private void setTime(String startTime, String endTime) {
		String start =checkTime(startTime);
		String end=checkTime(endTime);
		
		try {
			this.startWorkTime = LocalTime.parse(start, format);
			this.endWorkTime = LocalTime.parse(end, format);
		} catch (DateTimeParseException ex) {
			Logger.getGlobal().throwing(this.getClass().getName(), "setTime-initial time set", ex);
			System.out.println(ex.getMessage());
			setDefaultTime();
		}
		// check whether startTime is befor endTime
		if (this.startWorkTime.isAfter(this.endWorkTime)) {
			try {
				this.startWorkTime = this.endWorkTime;
				this.endWorkTime = LocalTime.parse(start, format);
			} catch (DateTimeParseException ex) {
				Logger.getGlobal().throwing(this.getClass().getName(), "setTime- reversing time", ex);
				setDefaultTime();
			}
		}

	}

	private String checkTime(String time) {
		int size=time.substring(0, time.indexOf(':')).trim().length();
		if(size==1) {
			return "0"+time.trim();
		}else {
			return time.trim();
		}
		
	}
	private void setDefaultTime() {
		try {
			this.startWorkTime = LocalTime.parse(DEFAULT_START_TIME, format);
			this.endWorkTime = this.startWorkTime.plusHours(MINIMAL_WORK_TIME);
		} catch (DateTimeParseException ex) {
			System.out.println(ex.getMessage());

		}
	}

	protected double getTotalTime() {
		return (this.endWorkTime.getHour()+(this.endWorkTime.getMinute()/MINUTES_PER_HOUR))
				- (this.startWorkTime.getHour()+(this.startWorkTime.getMinute()/MINUTES_PER_HOUR));
	}

	@Override
	public int compareTo(Employee o) {
		return Double.compare(o.getTotalTime(), this.getTotalTime());
	}

	public final ReadOnlyStringProperty getFirstNameProperty() {
		return firstNameProperty;
	}

	public final String getFirstName() {
		return firstNameProperty.get();
	}

	public final ReadOnlyStringProperty getLastNameProperty() {
		return lastNameProperty;
	}

	public final String getLastName() {
		return lastNameProperty.get();
	}

	public final IntegerProperty getRoomNbrProperty() {
		return roomNbrProperty;
	}

	public final Integer getRoomNbr() {
		return roomNbrProperty.get();
	}
	
	public final void setRoomNbr(int roomNbr) {
		this.roomNbrProperty.set(roomNbr);
	}

	public final String getStartWorkTime() {
		return startWorkTime.format(format);
	}

	public final String getEndWorkTime() {
		return endWorkTime.format(format);
	}

	@Override
	public String toString() {
		return String.format("%s %s %d %s %s%n",
				this.firstNameProperty.get(), this.lastNameProperty.get(), this.roomNbrProperty.get(),
				this.startWorkTime.format(format), this.endWorkTime.format(format));
	}

	@Override
	public int hashCode() {
		return this.firstNameProperty.hashCode() + this.lastNameProperty.hashCode() + this.roomNbrProperty.hashCode()
				+ this.startWorkTime.hashCode() + this.endWorkTime.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if(!this.getClass().equals(obj.getClass()))
			return false;
		Employee tmpEmpl=(Employee)obj;
		return this.firstNameProperty.getValue().equals(tmpEmpl.firstNameProperty.getValue())&&
				this.lastNameProperty.getValue().equals(tmpEmpl.lastNameProperty.getValue());
	}
}
