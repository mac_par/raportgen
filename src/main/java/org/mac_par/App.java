package org.mac_par;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;
import org.mac_par.controller.ReportGenController;
import java.io.FileInputStream;

public class App  extends Application{
	private ReportGenController raportGenController;
	
    @Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader fxmlLoader=new FXMLLoader();
		AnchorPane root = fxmlLoader.load(App.class.getResourceAsStream("/fxml/RaportViewFXML.fxml"));
		this.raportGenController=fxmlLoader.getController();
		this.raportGenController.setStage(primaryStage);
		primaryStage.setScene(new Scene(root));
		primaryStage.setTitle("Report Geneartor");
    	primaryStage.show();
	}

	@Override
	public void stop() throws Exception {
		raportGenController.close();
	}

	public static void main( String[] args )
    {
        launch(args);
    }
}
