package org.mac_par.view;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Scene;
import org.mac_par.controller.DialogPromptController;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;


public class DialogPrompt {
	private static DialogPromptController controller=null;
	private static FXMLLoader loader=null;
	private static AnchorPane pane=null;
	private static Stage stage=null;

	public static void displayInfo(String message) {
		if(controller==null)
		{
			if(!init())
				return;			
		}
	
		stage.setTitle("Info");
		controller.setInfoPrompt(message);
		stage.show();
	}
	public static void displayWarning(String message) {
		if(controller==null)
		{
			if(!init())
				return;			
		}
		
		stage.setTitle("Warning!");
		controller.setWarningPrompt(message);
		stage.show();
	}
	
	private static boolean init() {
		try {
		loader=new FXMLLoader(DialogPrompt.class.getResource("/fxml/DialogPromptFxml.fxml"));
		pane=loader.load();
		controller=loader.getController();
		stage=new Stage();
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setScene(new Scene(pane));
		}catch(IOException ex) {
			System.out.println("sth went wront "+ ex.getMessage());
			return false;
		}
		return true;
	}
	
	public static void close() {
		stage=null;
		pane=null;
		controller=null;
		loader=null;
	}
}
